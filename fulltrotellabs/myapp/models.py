from django.db import models
from django.contrib.auth.models import User,AbstractUser

# Create your models here.

class Timezone(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    time_zone = models.CharField(max_length=20,null=True, blank=True)

    class Meta:
        db_table = "time_zone"


class ActivityPeriod(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE, null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    end_time = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = "activity_periods"
