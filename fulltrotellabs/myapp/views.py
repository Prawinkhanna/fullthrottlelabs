from django.shortcuts import render
from .models import *
from django.http import JsonResponse, HttpResponse
import json
import datetime as datetime


# Create your views here.

def get_datetime_format(date_time):
    DB_datetome = str(date_time)
    splt_DateTime = DB_datetome.split('+')

    DateTime = datetime.datetime.strptime(splt_DateTime[0], '%Y-%m-%d %H:%M:%S').strftime('%b %d %y %I:%M:%S %p')
    return (DateTime)


def user_activity(request):
    try:
        user_timezone = Timezone.objects.all()
        activity_data = ActivityPeriod.objects.filter()
        data_json = dict()
        data_json["ok"] = True
        data_json["members"] = []
        for user_data in user_timezone:

            user_activity_data = activity_data.filter(user = user_data.user)
            activity_periods = []
            for data in user_activity_data:

                activity_json = {
                    "start_time": get_datetime_format(data.start_time),
                    "end_time": get_datetime_format(data.end_time),
                }
                activity_periods.append(activity_json)
            user_json = {
                "id": user_data.user.id,
                "real_name": user_data.user.first_name + user_data.user.last_name,
                "tz": user_data.time_zone,
                "activity_periods": activity_periods
            }
            data_json["members"].append(user_json)
        return JsonResponse(data_json,safe=False)
    except Exception as e:
        data_json = dict()
        data_json["ok"] = False
        data_json["error"] = e
        return JsonResponse(data_json, safe=False)





