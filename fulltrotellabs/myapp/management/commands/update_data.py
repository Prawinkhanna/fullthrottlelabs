from django.core.management.base import BaseCommand
from datetime import datetime
from django.contrib.auth.models import User
from myapp.models import *

def update_initial_data():

    user1 = User.objects.get_or_create(username='john',
                                 email='john@john.com',
                                 first_name= 'john',
                                 last_name = 'wick',
                                 password='john@john.com')

    timezone = Timezone.objects.get_or_create(time_zone='America/Los_Angeles', user=user1[0] )
    starttime = datetime(2020,4, 12, 12, 12, 12,0000)
    endtime = datetime(2020,4, 12, 12, 20, 12,0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user1[0])

    starttime = datetime(2020, 4, 14, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user1[0])

    starttime = datetime(2020, 4, 15, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 15, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user1[0])

    user2 = User.objects.get_or_create(username='jessy',
                                       email='jessy@jessy.com',
                                       first_name='jessy',
                                       last_name='aron',
                                       password='jessy@jessy.com')

    timezone = Timezone.objects.get_or_create(time_zone='America/Los_Angeles', user=user2[0])
    starttime = datetime(2020, 4, 12, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 12, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user2[0])

    starttime = datetime(2020, 4, 14, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user2[0])

    starttime = datetime(2020, 4, 15, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 15, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user2[0])

    user3 = User.objects.get_or_create(username='prawin',
                                       email='prawin@prawin.com',
                                       first_name='prawin',
                                       last_name='Khanna',
                                       password='prawin@prawin.com')

    timezone = Timezone.objects.get_or_create(time_zone='Asia/Kolkata', user=user3[0])
    starttime = datetime(2020, 3, 12, 12, 12, 12, 0000)
    endtime = datetime(2020, 3, 12, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user3[0])

    starttime = datetime(2020, 4, 14, 10, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 10, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user3[0])

    starttime = datetime(2020, 5, 1, 7, 12, 12, 0000)
    endtime = datetime(2020, 5, 1, 7, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user3[0])

    user4 = User.objects.get_or_create(username='john1',
                                       email='john1@john1.com',
                                       first_name='john1',
                                       last_name='wick',
                                       password='john1@john.com')

    timezone = Timezone.objects.get_or_create(time_zone='America/Los_Angeles', user=user4[0])
    starttime = datetime(2020, 4, 12, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 12, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user4[0])

    starttime = datetime(2020, 4, 14, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user4[0])

    starttime = datetime(2020, 4, 15, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 15, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user4[0])

    user5 = User.objects.get_or_create(username='jessy1',
                                       email='jessy1@jessy.com',
                                       first_name='jessy1',
                                       last_name='aron',
                                       password='jessy1@jessy.com')

    timezone = Timezone.objects.get_or_create(time_zone='America/Los_Angeles', user=user5[0])
    starttime = datetime(2020, 4, 12, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 12, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user5[0])

    starttime = datetime(2020, 4, 14, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user5[0])

    starttime = datetime(2020, 4, 15, 12, 12, 12, 0000)
    endtime = datetime(2020, 4, 15, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user5[0])

    user6 = User.objects.get_or_create(username='prawin1',
                                       email='prawin@prawin.com',
                                       first_name='prawin',
                                       last_name='Khanna',
                                       password='prawin@prawin.com')

    timezone = Timezone.objects.get_or_create(time_zone='Asia/Kolkata', user=user6[0])
    starttime = datetime(2020, 3, 12, 12, 12, 12, 0000)
    endtime = datetime(2020, 3, 12, 12, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user6[0])

    starttime = datetime(2020, 4, 14, 10, 12, 12, 0000)
    endtime = datetime(2020, 4, 14, 10, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user6[0])

    starttime = datetime(2020, 5, 1, 7, 12, 12, 0000)
    endtime = datetime(2020, 5, 1, 7, 20, 12, 0000)

    activity_data = ActivityPeriod.objects.get_or_create(start_time=starttime, end_time=endtime, user=user6[0])


class Command(BaseCommand):
    def handle(self, **options):
        update_initial_data()
