This is the assesment given by FullThrottle Labs

This project has been hosted in heroku

link: https://fullthrottlelabsbyprawin.herokuapp.com/activity_details/

Follow the below steps to run this project:
1. clone this project by using git clone https://gitlab.com/Prawinkhanna/fullthrottlelabs.git
2. activate the virtual environment
3. install all requirements by using pip3 install -r requirements.txt
4. open terminal to excute the upcomming commands:
5. python manage.py makemigrations (To generate the sql qurries from the model)
6. python manage.py migrate (To execute the sql command in database)
7. python manage.py update_data (A custom management command to populate the database with some dummy data)
8. python manage.py runserver (To run this project in local)
9. Then finally use this endpoint to get the required output http://127.0.0.1:8000/activity_details/

Note : 
    1. Models used in project are User model (imported the auth.models),ActivityPeriod and Timezone
    2. This project has been configured for MySql and SQLite databases, To execute 
    this project in MySql database it need to be uncomment the commented MySql 
    connection string from settings.
